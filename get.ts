// https://www.tomas-dvorak.cz/posts/nodejs-request-without-dependencies/
// import https from 'https';
const http = require('http');

export function get(url: string): Promise<string> {
  return new Promise((resolve, reject) => {
    // const lib = url.startsWith('https') ? https : http;
    const request = http.get(url, (response: any) => {
      if (response.statusCode < 200 || response.statusCode > 299) {
        reject(new Error('Failed to load page, status code: ' + response.statusCode));
      }
      const body: string[] = [];
      response.on('data', (chunk: any) => body.push(chunk));
      response.on('end', () => resolve(body.join('')));
    });
    request.on('error', (err: string) => reject(err));
  });
}
