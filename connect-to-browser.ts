import puppeteer from 'puppeteer';

import { get } from './get';

export async function connectToBrowser() {
  let browser: puppeteer.Browser;
  let isNewBrowserInstance = false;

  try {
    const browserInfo = await get('http://localhost:9222/json/version');
    const { webSocketDebuggerUrl } = JSON.parse(browserInfo);
    console.log('Connecting to existing browser on', webSocketDebuggerUrl);
    browser = await puppeteer.connect({browserWSEndpoint: webSocketDebuggerUrl});
  }
  catch(err) {
    console.log('Launching new browser instance');
    isNewBrowserInstance = true;
    browser = await puppeteer.launch();
  }

  return {browser, isNewBrowserInstance};
}
