import { Browser } from 'puppeteer';

import { connectToBrowser } from './connect-to-browser';

export async function frame(functionToRunInBrowser: (browser: Browser) => Promise<string>) {
  const { browser, isNewBrowserInstance } = await connectToBrowser();

  const result = await functionToRunInBrowser(browser);

  if (isNewBrowserInstance) {
    await browser.close();
  } else {
    browser.disconnect();
  }

  return result;
}
