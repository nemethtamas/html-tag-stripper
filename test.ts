import fileUrl from 'file-url';
import { writeFile } from 'fs';
import { promisify } from 'util';
import HTMLMinifier from 'html-minifier';

const writeFileAsync = promisify(writeFile);

import { stripHtmlTags } from './index';

(async () => {
  const optimizedHtml = await stripHtmlTags(fileUrl('2018-04-29.html'), {
    attributeWhitelist: []
  });
  const minifiedHtml = HTMLMinifier.minify(optimizedHtml, {
    collapseWhitespace: true
  });
  await writeFileAsync('dist/generated.html', minifiedHtml, 'utf8');
})();
