import { HTMLStripperConfiguration } from './html-stripper-configuration';
import { frame } from './frame';

export async function stripHtmlTags(url: string, stripperConfiguration: HTMLStripperConfiguration) {
  return frame(async (browser) => {
    const page = await browser.newPage();
    await page.goto(url);

    const contents = await page.evaluate((configuration) => {
      const allElements = document.body.querySelectorAll('*');

      for (const element of allElements) {
        removeBlacklistedAttributesOnElement(element);
        removeElementIfEmpty(element);
      }

      return document.body.innerHTML;

      function removeBlacklistedAttributesOnElement(element: Element) {
        for (const attributeName of element.getAttributeNames()) {
          if (!configuration.attributeWhitelist.includes(attributeName)) {
            element.removeAttribute(attributeName);
          }
        }
      }

      function removeElementIfEmpty(element: Element) {
        const elementContent = element.textContent;
        if (elementContent === null || elementContent.trim() === '') {
          element.remove();
        }
      }
    }, stripperConfiguration);

    await page.close();

    return contents;
  });
}
